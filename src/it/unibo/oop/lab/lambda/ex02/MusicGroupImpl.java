package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return this.songs.stream().map(Song::getSongName).sorted();
        /*
        final Set<String> osongs = new HashSet<>();
        this.songs.forEach(song -> {
            osongs.add(song.getSongName());
        });
        return osongs.stream().sorted();
        */
    }

    @Override
    public Stream<String> albumNames() {
        /*
        final Set<String> oalbums = new HashSet<>();
        this.albums.forEach((k, v) -> {
            oalbums.add(k);
        });
        return oalbums.stream().sorted();
        */
        return this.albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
       return this.albums.entrySet().stream()
            .filter(entry -> entry.getValue().intValue() == year)
            .map(Entry::getKey);

//        final Set<String> album = new HashSet<>();
//        this.albums.forEach((k, v) -> {
//            if (v.intValue() == year) {
//                album.add(k);
//            }
//        });
//        return album.stream();
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) this.songs.stream()
                .map(Song::getAlbumName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(album -> album.equals(albumName))
//                .filter(song -> song.getAlbumName().isPresent())
//                .filter(song -> song.getAlbumName().get().equals(albumName))
                .count();
       /* int count = 0;
        for (final Song song : this.songs) {
            if (song.getAlbumName().isPresent() && song.getAlbumName().get().equals(albumName)) {
                count++;
            }
        }
        return count;*/
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) this.songs.stream()
            .map(Song::getAlbumName)
            .filter(album -> !album.isPresent())
            .count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return this.songs.stream()
            .filter(song -> song.getAlbumName().isPresent())
            .filter(song -> song.getAlbumName().get().equals(albumName))
            .mapToDouble(Song::getDuration)
            .average();
//        double duration = 0;
//        for (final Song song : this.songs) {
//            if (song.getAlbumName().isPresent() && song.getAlbumName().get().equals(albumName)) {
//                duration += song.duration;
//            }
//        }
//        return OptionalDouble.of(duration / this.countSongs(albumName));
    }

    @Override
    public Optional<String> longestSong() {
        return this.songs.stream()
                .max((song1, song2) -> Double.compare(song1.getDuration(), song2.getDuration()))
                .map(Song::getSongName);
//            .filter(song -> song.getAlbumName().isPresent())
//            .sorted()
//            .findFirst()
//            .get()
//            .getAlbumName();
        /*
        Song maxdurationsong = new Song("", Optional.of(""), 0);
        for (final Song song : this.songs) {
            if (song.getAlbumName().isPresent() && song.getDuration() > maxdurationsong.getDuration()) {
                maxdurationsong = new Song(song.getSongName(), song.getAlbumName(), song.getDuration());
            }
        }
        return maxdurationsong.getAlbumName();*/
    }

    @Override
    public Optional<String> longestAlbum() {
        return this.songs.stream()
               .filter(song -> song.getAlbumName().isPresent())         //Filtro solo quelli presenti
               .collect(Collectors.groupingBy(Song::getAlbumName,       //Li colleziono in una mappa di <Optional<String>, Double>
                       Collectors.summingDouble(Song::getDuration)))    //e sommo le durate che hanno la stessa chiave
               .entrySet().stream()
               .collect(Collectors.maxBy((d1, d2) -> Double.compare(d1.getValue(), d2.getValue())))     //Estraggo il  massimo dalla mappa
               .get().getKey();                                                                         //Ottengo la chiave (il nome dell'album) 
        /*                                                                                              //che ha maggior durata
        final Map <String, Double> al = new HashMap<>();
        for (final Song song : this.songs) {
            if (song.albumName.isPresent() && !al.containsKey(song.albumName.get())) {
                al.put(song.getAlbumName().get(), song.duration);
            } else if (song.albumName.isPresent() && al.containsKey(song.albumName.get())) {
                al.replace(song.albumName.get(), al.get(song.albumName.get()), 
                        al.get(song.albumName.get()) + song.duration);
            }
        }
        String key = "";
        double max = 0;
        for (final String k : al.keySet()) {
            if (al.get(k).doubleValue() > max) {
                max = al.get(k).doubleValue();
                key = k;
            }
        }
        return  Optional.of(key);
        */
    }
    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        private Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }
        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }
        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }
        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }
    }
}
